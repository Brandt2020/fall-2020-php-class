<?php
    if(isset($_POST['movie_name']) && !empty($_POST['movie_name']) 
        && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])){
        
        //echo"<pre>";print_r($_POST);echo"</pre>";exit;
        
        $title = $_POST['movie_name'];
        $rating = $_POST['movie_rating'];

        //--database stuff
        include('../includes/db_con.php');
        try{
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("
                INSERT INTO
                  phpclass.movielist (Movie_Title, Movie_Rating)
                VALUE (:Title, :Rating)
            ");
                $sql->bindValue(':Title', $title);
                $sql->bindValue(':Rating', $rating);
                $sql->execute();

                header("Location:list.php?success=1");
                exit('DB Write Successful');
        }catch(PDOException $e){
            echo "Error: ".$e->getMessage();
            exit;
        }
    }
    else if (isset($_POST['movie_submit']))
        {
            $error = "Please ensure you have added both a title and rating before submitting the new movie";
        }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Movies</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <?php if(isset($error) && !empty($error)):?>
        <h1 style="color: red;"><?= $error ?></h1>

    <?php endif; ?>
    <form method="post">
        <table border="1" height="80%">
            <tr height="100">
                <th colspan="2"><h3>Add New Movie</h3></th>
            </tr>
            <tr height="40">
                <th>Movie Name</th>
                <td align="left"><input type="text" size="50" name="movie_name" id="movie_name" required/></td>
            </tr>
            <tr height="40">
                <th>Movie Rating</th>
                <td align="left"><input type="text" size="10" name="movie_rating" id="movie_rating" required/></td>
            </tr>
            <tr height="40">
                <td colspan="2"><input name="movie_submit" id="movie_submit" type="submit"></td>
            </tr>
        </table>

    </form>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>