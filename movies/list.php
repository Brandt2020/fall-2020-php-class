<?php
include ('../includes/db_con.php');
try{
    $db = new PDO($db_dsn, $db_username, $db_password, $db_options); // Calling pizza hut
    $sql = $db->prepare("Select * from phpclass.movielist;"); // Placing the order
    $sql -> execute(); // Baking the pizza
    $movie_list = $sql -> fetchAll(); // pizza delivered
    //print_r($rows); // consuming pizza
    //exit;
}catch(PDOException $e)
{
    echo $e ->getMessage();
    exit;
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zach's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>

    <h3> My Movie List </h3>

    <?php if(isset($_GET['success'])): ?>
        <p style="color:green;"><strong>New Movie Added!</strong></p>
    <?php endif; ?>

    <table border="1" width="60%" >
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Rating</th>
        </tr>
            <?php foreach($movie_list as $movie ): ?>
        <tr>
            <td><?= $movie['Movie_ID'] ?></td>
            <td><?= $movie['Movie_Title'] ?></td>
            <td><?= $movie['Movie_Rating'] ?></td>
        </tr>
            <?php endforeach; ?>

    </table>

    <br/><br/>
    <a href="add.php">Add a New Movie</a>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>