<?php
    class Car
    {
        //--Define Properties
        public $color;
        public $make;
        public $model;
        public $year;
        public $status;

        /**
         * Car constructor.
         * @param string $status the motion status of the car. (Forward, Reverse, Stopped)
         */
        function __construct($status)
        {
            $this->status = $status;
        }

        /**
         * Put car in forward
         */
        function forward()
        {
            echo "The car is moving forward.";
            $this->status = 'forward';
        }

        /**
         * Put car in reverse
         */
        function reverse()
        {
            echo "The car is moving in reverse.";
            $this->status = 'reverse';
        }

        /**
         * Stop the car
         */
        function stop()
        {
            echo "The car is stopped.";
            $this->status = 'stopped';
        }

        /**
         * @return mixed
         */
        public function getColor()
        {
            return $this->color;
        }

        /**
         * @param mixed $color
         */
        public function setColor($color)
        {
            $this->color = $color;
        }

        /**
         * @return mixed
         */
        public function getMake()
        {
            return $this->make;
        }

        /**
         * @param mixed $make
         */
        public function setMake($make)
        {
            $this->make = $make;
        }

        /**
         * @return mixed
         */
        public function getModel()
        {
            return $this->model;
        }

        /**
         * @param mixed $model
         */
        public function setModel($model)
        {
            $this->model = $model;
        }

        /**
         * @return mixed
         */
        public function getYear()
        {
            return $this->year;
        }

        /**
         * @param mixed $year
         */
        public function setYear($year)
        {
            $this->year = $year;
        }

        /**
         * @return string
         */
        public function getStatus()
        {
            return $this->status;
        }

        /**
         * @param string $status
         */
        public function setStatus($status)
        {
            $this->status = $status;
        }

    }
        //------------\\
        $my_car = new Car();
        $my_car ->color = 'Green';
        $my_car->setMake('Honda');
        $my_car->setModel('Accord');
        $my_car->setYear('2018');

        $my_neighbor_car = new Car();
        $my_neighbor_car ->color = 'Blue';
        $my_neighbor_car->setMake('Toyota');
        $my_neighbor_car->setModel('Camry');
        $my_neighbor_car->setYear('2010');




?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zach's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
<h1>PHP Class Example - Car Class</h1>
    <?php
    print_r($my_car);
    $my_car->forward();
    $my_car->reverse();
    $my_car->stop();

    ?>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>