<?php
if(isset($_POST['customerID']) && !empty($_POST['customerID'])
    && isset($_POST['firstName']) && !empty($_POST['firstName'])
    && isset($_POST['lastName']) && !empty($_POST['lastName'])
    && isset($_POST['address']) && !empty($_POST['address'])
    && isset($_POST['city']) && !empty($_POST['city'])
    && isset($_POST['state']) && !empty($_POST['state'])
    && isset($_POST['zip']) && !empty($_POST['zip'])
    && isset($_POST['phone']) && !empty($_POST['phone'])
    && isset($_POST['email']) && !empty($_POST['email'])
    && isset($_POST['password']) && !empty($_POST['password'])){

    //echo"<pre>";print_r($_POST);echo"</pre>";exit;

    $customerID = $_POST['customerID'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    //--database stuff

    include('../includes/db_con.php');
    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("
                INSERT INTO
                  phpclass.customer (CustomerID, FirstName, LastName, Address, City, State, Zip, Phone, Email, Password)
                VALUE (:CustomerID, :FirstName, :LastName, :Address, :City, :State, :Zip, :Phone, :Email, :Password)
            ");
        $sql->bindValue(':CustomerID', $customerID);
        $sql->bindValue(':FirstName', $firstName);
        $sql->bindValue(':LastName', $lastName);
        $sql->bindValue(':Address', $address);
        $sql->bindValue(':City', $city);
        $sql->bindValue(':State', $state);
        $sql->bindValue(':Zip', $zip);
        $sql->bindValue(':Phone', $phone);
        $sql->bindValue(':Email', $email);
        $sql->bindValue(':Password', $password);
        $sql->execute();

        header("Location:customer_view.php?success=1");
        exit('DB Write Successful');
    }catch(PDOException $e){
        echo "Error: ".$e->getMessage();
        exit;
    }
}
else if (isset($_POST['customer_submit']))
{
    $error = "Please ensure you have added all fields before submitting customer";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create New Customer</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
    <form method="post">
        <table border="1" height="80%">
            <tr height="100">
                <th colspan="2"><h3>Add New Customer</h3></th>
            </tr>
            <tr height="40">
                <th>Customer ID</th>
                <td align="left"><input type="text" size="50" name="customer_id" id="customer_id" required/></td>
            </tr>
            <tr height="40">
                <th>First Name</th>
                <td align="left"><input type="text" size="50" name="first_name" id="first_name" required/></td>
            </tr>
            <tr height="40">
                <th>Last Name</th>
                <td align="left"><input type="text" size="50" name="last_name" id="last_name" required/></td>
            </tr>
            <tr height="40">
                <th>Address</th>
                <td align="left"><input type="text" size="50" name="address" id="address" required/></td>
            </tr>
            <tr height="40">
                <th>City</th>
                <td align="left"><input type="text" size="50" name="city" id="city" required/></td>
            </tr>
            <tr height="40">
                <th>State</th>
                <td align="left"><input type="text" size="50" name="state" id="state" maxlength="2"  required/></td>
            </tr>
            <tr height="40">
                <th>Zip</th>
                <td align="left"><input type="text" size="50" name="zipcode" id="zipcode" maxlength="5" required/></td>
            </tr>
            <tr height="40">
                <th>Phone</th>
                <td align="left"><input type="tel" size="50" name="phone_number" id="phone_number" required/></td>
            </tr>
            <tr height="40">
                <th>Email</th>
                <td align="left"><input type="email" size="50" name="email_address" id="email_address" required/></td>
            </tr>
            <tr height="40">
                <th>Password</th>
                <td align="left"><input type="password" size="50" name="password" id="password" required/></td>
            </tr>
            <tr height="40">
                <td colspan="2"><input name="customer_submit" id="customer_submit" type="submit"></td>
            </tr>
        </table>

    </form>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>