<?php
include ('../includes/db_con.php');
try{
    $db = new PDO($db_dsn, $db_username, $db_password, $db_options); // Calling pizza hut
    $sql = $db->prepare("Select * from phpclass.customer;"); // Placing the order
    $sql -> execute(); // Baking the pizza
    $customer_list = $sql -> fetchAll(); // pizza delivered
    //print_r($rows); // consuming pizza
    //exit;
}catch(PDOException $e)
{
    echo $e ->getMessage();
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>View Customer Listing</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>

    <h3> My Customer List </h3>

    <?php if(isset($_GET['success'])): ?>
        <p style="color:green;"><strong>New Customer Added!</strong></p>
    <?php endif; ?>

    <table border="1" width="60%" >
        <tr>
            <th>ID</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>
        <?php foreach($customer_list as $customer ): ?>
            <tr>
                <td><?= $customer['CustomerID'] ?></td>
                <td><?= $customer['FirstName'] ?></td>
                <td><?= $customer['LastName'] ?></td>
                <td><?= $customer['Address'] ?></td>
                <td><?= $customer['City'] ?></td>
                <td><?= $customer['State'] ?></td>
                <td><?= $customer['Zip'] ?></td>
                <td><?= $customer['Phone'] ?></td>
                <td><?= $customer['Email'] ?></td>
                <td><?= $customer['Password'] ?></td>
            </tr>
        <?php endforeach; ?>

    </table>

    <br/><br/>
    <a href="customer_db.php">Add a New Customer?</a>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>