<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Strings and Loops</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
<?php

//-------------------------------------------\\
    /*$number = 101;
    $result = "<h1>";
    $result .= $number;
    $result .= "</h1>";

    echo $result;

    //echo "<h1>".$number."</h1>";

    echo "<h1>";
    echo $numr;
    echo "</h1>";

    $number1= 100;
    $number2 = "55";
    $number = $number1 + $number2;

    echo "<h1>".$number."</h1>";*/

//-------------------------------------------\\
    $i = 1;
    while ($i<7) {
        echo "<h$i>Hello World</h$i>";
        $i++;
    }

    $i = 6;
    do {
        echo "<h$i>Hello World</h$i>";
        $i--;
    }while ($i>0);

    for($i = 1; $i<7; $i++)
    {
        echo "<h$i>Hello World</h$i>";
    }

//-------------------------------------------\\
    echo"<br /><br /><hr><br />";
    $fullName = "Doug Smith";

    //D o u g  _S m i t h
    //0 1 2 3 4 5 6 7 8 9

    $Position = strpos($fullName, ' ');

    echo $Position;
    echo"<br /><br /><hr><br />";

//-------------------------------------------\\
    echo"<br /><br /><hr><br />";

    echo $fullName;
    echo "<br />";
    $fullName = strtoupper($fullName);
    echo $fullName;

    echo"<br /><br /><hr><br />";
//-------------------------------------------\\
    echo"<br /><br /><hr><br />";

    echo $fullName;
    echo "<br />";
    $fullName = strtolower($fullName);
    echo $fullName;

    echo"<br /><br /><hr><br />";

//-------------------------------------------\\

    $nameParts = explode(' ', $fullName);
    echo $nameParts[0];
    echo "<br />";
    echo $nameParts[1];

//-------------------------------------------\\


?>

</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>