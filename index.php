<!doctype html>
    <html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zach's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css">
</head>

<body>

<header><?php include 'includes/header.php'?></header>

<nav><?php include 'includes/nav.php'?></nav>

<main>
    <img src="images/zach.jpg" alt="Picture of Zach"/>
    <p>
        Hello, my name is Zachary Brandt. I am a student in the Software Developer program. My family moved to Appleton
        when I was 4 and I have lived in the area ever since. My hobbies include playing music on my guitar, playing
        Magic: The Gathering, and spending time outdoors. I have a 1 year old cat named Chi, who is in the picture with
        me. I hope to one day have a job where I can directly help people and support the local community.
    </p>
</main>

<footer><?php include "includes/footer.php" ?></footer>

</body>
</html>