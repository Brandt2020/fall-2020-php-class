<?php

$userNum1 = mt_rand(1,6);
$userNum2 = mt_rand(1,6);
$comNum1 = mt_rand(1,6);
$comNum2 = mt_rand(1,6);
$comNum3 = mt_rand(1,6);

$dice = array();
$dice[0]= "../images/dice_1.png";
$dice[1]= "../images/dice_2.png";
$dice[2]= "../images/dice_3.png";
$dice[3]= "../images/dice_4.png";
$dice[4]= "../images/dice_5.png";
$dice[5]= "../images/dice_6.png";

$userResult = ($userNum1 + $userNum2);
$comResult = ($comNum1 + $comNum2 + $comNum3);
$winner = "";

if ($userResult==$comResult)
{
    $winner = "Result: Tie";
}
elseif($userResult>$comResult)
{
    $winner = "Result: You Win!";
}
else
{
    $winner = "Result: Computer Wins";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dice Game Demo</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php'?></header>
<nav><?php include '../includes/nav.php'?></nav>
<main>
<h3>Your Score:<?=$userResult?></h3>
    <img src="<?=$dice[$userNum1 - 1]?>">
    <img src="<?=$dice[$userNum2 - 1]?>">
    <h3>Computers Score:<?=$comResult?></h3>
    <img src="<?=$dice[$comNum1 - 1]?>">
    <img src="<?=$dice[$comNum2 - 1]?>">
    <img src="<?=$dice[$comNum3 - 1]?>">
    <h2><?=$winner?></h2>
</main>
<footer><?php include "../includes/footer.php" ?></footer>
</body>
</html>